package com.company;

import java.util.Timer;
import java.util.TimerTask;

public class Main {



    public static void main(String[] args) {

        Menu menu = new Menu();

        Timer timer = new Timer();
        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                menu.run();
            }
        };
        // Refresh app window
       timer.scheduleAtFixedRate(timerTask,3000,3000);


    }






}
