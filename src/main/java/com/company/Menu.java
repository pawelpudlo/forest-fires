package com.company;

import DTO.Data;
import DTO.DronDto;
import DTO.FireData;
import org.json.simple.JSONObject;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Menu {

    private JFrame frame = new JFrame();
    private ImageProcessing imageProcessing = new ImageProcessing();
    Random rn = new Random();



    public Menu() {
        imageProcessing.setVisible(true);
        frame.add(imageProcessing);

        frame.setResizable(false);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        frame.setTitle("ForestFira map api");

        frame.pack();
        frame.repaint();
    }


    public void run() {
        //generate example 2d array


        JSON jsonData = new JSON("Data.json");
        JSON jsonDron = new JSON("Dron.json");
        Data data;
        DronDto dronDto;

        List dataRegion = new ArrayList();
        List dataDron = new ArrayList();
        List dataJson = jsonData.getJson(); // Lista przymująca wszystkie dane w formacie JSON
        List dronJson = jsonDron.getJson(); // Lista przyjmująca dane na temat dronów
        int id;
        int a;
        for(int i = 0; i < 10; i++)
        {
            a = 0;
            JSONObject dronInfo = (JSONObject) dronJson.get(i);

            boolean status = Boolean.getBoolean(dronInfo.get("status").toString());
            int battery_level = Integer.parseInt(dronInfo.get("battery_level").toString());
            boolean failure = Boolean.getBoolean(dronInfo.get("failure").toString());
            boolean loading = Boolean.getBoolean(dronInfo.get("loading").toString());
            int start_place = Integer.parseInt(dronInfo.get("start_place").toString());
            int current_place = Integer.parseInt(dronInfo.get("current_place").toString());
            int final_place = Integer.parseInt(dronInfo.get("final_place").toString());
            for( int j = 0; j < 10; j++)
            {
                id= i*10 + j;

                JSONObject dataArea = (JSONObject) dataJson.get(id);


                int id_json = Integer.parseInt(dataArea.get("id").toString());
                int state_json = Integer.parseInt(dataArea.get("state").toString());
                float temperature_json = Float.parseFloat(dataArea.get("temperature").toString()) ;


                FireData fireData = new FireData(temperature_json,0,0,id_json,state_json);


                if(battery_level < 20 && a==0){
                    loading = true;
                }

                if(current_place == id && a==0 && !loading){

                    fireData.setX(i);
                    fireData.setY(j);
                    fireData.setStatus(rn.nextInt(10) % 10);
                    fireData.setId(i * 10 + j);
                    fireData.setTemperature(changeTemperature(temperature_json)); // Ustawianie Temperatury w obiekcie

                    current_place++;
                    if(current_place > final_place){
                        current_place = start_place;
                    }
                    battery_level--;
                    a=1;
                }

                if (loading && a==0){
                    if(current_place > start_place){
                        current_place--;
                        battery_level--;
                    }else if(current_place == start_place){
                        battery_level += 10;
                    }else{
                        current_place = start_place;
                    }

                    if(battery_level >= 100){
                        battery_level = 100;
                        loading = false;
                    }
                }

                data = new Data(fireData.getId(),fireData.getStatus(),fireData.getTemperature()); // Obiekt który dodajemy do listy w celu stworzenia JSONA
                dataRegion.add(data);



                imageProcessing.setData(i, j, fireData);
            }
            dronDto = new DronDto(i,status,battery_level,failure,loading,start_place,final_place,current_place);
            dataDron.add(dronDto);

        }

        jsonData.writeJson(dataRegion);
        jsonDron.writeJson(dataDron);

        imageProcessing.repaint();

    }

    private float changeTemperature(float temperature){

        int status = rn.nextInt(40);

        if(temperature >= 40) status = 1;

        switch (status){
            case 0:
                return temperature;
            case 1:
                if(temperature < 40){
                    return temperature + 100;
                }else if (temperature >= 800 && temperature <= 1200){
                    return 800 + rn.nextInt(400);
                }else{
                    return temperature + 200;
                }
            case 2:
                return temperature + rn.nextInt(10);
            case 3:
                if(temperature > 10 && temperature < 400) temperature = 20;
                    return temperature;
            default:
                return temperature + rn.nextInt(4) - rn.nextInt(3);
        }

    }

}
