package com.company;

import DTO.AllTeamDto;
import DTO.ListData;
import DTO.TeamDto;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class JSON {

    private String filePath;
    JSONParser jsonParser = new JSONParser();

    public JSON(String dataName) {
        this.filePath = dataName;
    }

    public void writeJson(List listJson){


            try (Writer writer = new FileWriter(filePath)) {

                Gson gson = new GsonBuilder().create();

                if(this.filePath.equals("Data.json") || this.filePath.equals("Dron.json")){
                    ListData listData = new ListData(listJson);
                    gson.toJson(listData,writer);
                }


            }catch (IOException e) {
                e.printStackTrace();
                System.out.println("x");
            }
    }

    public void writeJsonTeam(TeamDto fireDto, TeamDto patrolDto){
        try (Writer writer = new FileWriter(filePath)) {

            Gson gson = new GsonBuilder().create();

            if(this.filePath.equals("Team.json")){

                AllTeamDto allTeamDto = new AllTeamDto(fireDto,patrolDto);

                gson.toJson(allTeamDto,writer);

            }
        }catch (IOException e) {
            e.printStackTrace();
            System.out.println("x");
        }
    }

    public List getJson(){

        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(filePath))
        {
            //Read JSON file
            JSONObject obj = (JSONObject) jsonParser.parse(reader);

            ArrayList jsonList = new ArrayList();

            if(filePath.equals("Data.json") || filePath.equals("Dron.json")){
                jsonList = (ArrayList) obj.get("Data");
            }else{
                jsonList.add(obj.get("teamFire"));
                jsonList.add(obj.get("teamPatrol"));
            }

            return jsonList;

        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }


        return new ArrayList();
    }

    public String getFilePath() {
        return filePath;
    }
}
