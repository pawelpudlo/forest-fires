package com.company;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.json.simple.JSONObject;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

public class CreatorPDF {

    public CreatorPDF(List data) {
        createPdf(data);
    }

    private void  createPdf(List data){
            try{
                LocalDate localDate = LocalDate.now();
                LocalTime localTime = LocalTime.now();

                String hours = "";
                String min = "";
                String secound = "";


                if(localTime.getHour() < 10){
                    hours = "0" + localTime.getHour();
                }else{
                    hours = String.valueOf(localTime.getHour());
                }

                if(localTime.getMinute() < 10){
                    min = "0" + localTime.getMinute();
                }else{
                    min = String.valueOf(localTime.getMinute());
                }

                if(localTime.getSecond() < 10){
                    secound = "0" + localTime.getSecond();
                }else{
                    secound = String.valueOf(localTime.getSecond());
                }


                String fullDate = String.format("%s %s:%s:%s",localDate.toString(),hours,min,secound);
                String file = String.format("%s %s-%s-%s",localDate.toString(),hours,min,secound);



                Document document = new Document();
                PdfWriter.getInstance(document,new FileOutputStream(String.format("./raports/%s.pdf",file)));
                document.open();

                Font fTitle = new Font(Font.FontFamily.TIMES_ROMAN,32,Font.BOLD,BaseColor.BLACK);
                Font f2 = new Font(Font.FontFamily.UNDEFINED,8,Font.NORMAL,BaseColor.BLACK);

                Paragraph para = new Paragraph("Forest region report \n ",fTitle);
                Paragraph paraTime = new Paragraph("Time: "+ fullDate + "\n ");
                Paragraph paraInfo = new Paragraph("\n Forest Fires",f2);
                para.setAlignment(Element.ALIGN_CENTER);
                paraTime.setAlignment(Element.ALIGN_CENTER);
                paraInfo.setAlignment(Element.ALIGN_RIGHT);

                document.add(para);
                document.add(paraTime);



                PdfPTable  table = new PdfPTable(2);

                table.setWidthPercentage(100);
                table.setSpacingBefore(0f);
                table.setSpacingAfter(0f);

                PdfPCell cell = new PdfPCell(Phrase.getInstance("Region id"));
                cell.setFixedHeight(25);
                cell.setPadding(5);
                cell.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(cell);

                cell.setPhrase(Phrase.getInstance("Temperature"));
                table.addCell(cell);


                for (Object datum : data) {
                    JSONObject dataArea = (JSONObject) datum;

                    String id_json = dataArea.get("id").toString();
                    String temperature_json = dataArea.get("temperature").toString();
                    cell.setPhrase(Phrase.getInstance(id_json));
                    table.addCell(cell);
                    cell.setPhrase(Phrase.getInstance(temperature_json));
                    table.addCell(cell);
                }


                document.add(table);
                document.add(paraInfo);

                document.close();

                System.out.println(String.format("You create raport of Data: %s Time: %s:%s:%s",localDate.toString(),hours,min,secound));
            } catch (FileNotFoundException | DocumentException e) {
                System.out.println(e);
            }
        }

}
