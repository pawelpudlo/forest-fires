package com.company;

public class Dron {

    private int battery_level;
    private boolean loading;
    private boolean failure;
    private boolean status;
    private int idDron;
    private int start_place;
    private int final_place;


    public Dron(int idDron,int s_place,int f_place) {
        this.idDron = idDron;
        this.start_place = s_place;
        this.final_place = f_place;

    }

    public int getBattery_level() {
        return battery_level;
    }

    public boolean isLoading() {
        return loading;
    }

    public boolean isFailure() {
        return failure;
    }

    public boolean isStatus() {
        return status;
    }

    public void setBattery_level(int battery_level) {
        this.battery_level = battery_level;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setFailure(boolean failure) {
        this.failure = failure;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public int getIdDron() {
        return idDron;
    }

    public int getStart_place() {
        return start_place;
    }

    public int getFinal_place() {
        return final_place;
    }

    public void setStart_place(int start_place) {
        this.start_place = start_place;
    }

    public void setFinal_place(int final_place) {
        this.final_place = final_place;
    }
}
