package com.company;

import DTO.AllTeamDto;
import DTO.Data;
import DTO.FireData;
import DTO.TeamDto;
import org.json.simple.JSONObject;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.lang.reflect.Array;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

public class ImageProcessing extends JPanel implements ActionListener, MouseListener {

    private int imageSizeX = 1000, imageSizeY = 750, gridSizeX = 10, gridSizeY = 10;
    private int differenceX = imageSizeX / gridSizeX, differenceY = imageSizeY / gridSizeY;
    private final int winSizeX = 1200, winSizeY = 750;
    private FireData data_array[][] = new FireData[gridSizeX][gridSizeY];
    private int selectedX = -1, selectedY = -1;
    private int statusLimit = 8;
    private int temperatureLimit = 200;

    private JSON json;
    private JSON jsonData;
    private JSON jsonData2;
    private List jsonList;
    private JSONObject patrolList;
    private JSONObject fireTeamList;

    private int available_p;
    private int action_p;
    private int all_action_p;

    private int available_f;
    private int action_f;
    private int all_action_f;

    private JLabel text = new JLabel();
    private JLabel textFireTeam = new JLabel();
    private JLabel textPatrolTeam = new JLabel();
    private JLabel textTogether = new JLabel();

    private String link = "resources/forest.png";
    private BufferedImage forest;

    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;

    public ImageProcessing() {

        this.json = new JSON("Team.json");
        this.jsonData = new JSON("Data.json");
        this.jsonData2 = new JSON("Data.json");
        this.jsonList = new ArrayList(json.getJson());
        this.patrolList = (JSONObject) this.jsonList.get(1);
        this.fireTeamList = (JSONObject) this.jsonList.get(0);

        this.available_p = Integer.parseInt(patrolList.get("units").toString());
        this.action_p = Integer.parseInt(patrolList.get("action").toString());
        this.all_action_p = Integer.parseInt(patrolList.get("all_action").toString());

        this.available_f = Integer.parseInt(fireTeamList.get("units").toString());
        this.action_f = Integer.parseInt(fireTeamList.get("action").toString());
        this.all_action_f = Integer.parseInt(fireTeamList.get("all_action").toString());

        this.addMouseListener(this);
        this.setLayout(null);

        try {
            forest = ImageIO.read(new File(link));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }

        text.setBounds(1000, 0, 200, 150);
        text.setHorizontalAlignment(SwingConstants.CENTER);
        text.setVerticalAlignment(SwingConstants.CENTER);
        this.add(text);

        textFireTeam.setBounds(1000, 200, 200, 150);
        textFireTeam.setHorizontalAlignment(SwingConstants.LEFT);
        textFireTeam.setVerticalAlignment(SwingConstants.CENTER);
        this.add(textFireTeam);

        textPatrolTeam.setBounds(1000, 300, 200, 150);
        textPatrolTeam.setHorizontalAlignment(SwingConstants.LEFT);
        textPatrolTeam.setVerticalAlignment(SwingConstants.CENTER);
        this.add(textPatrolTeam);

        textTogether.setBounds(1000, 400, 200, 150);
        textTogether.setHorizontalAlignment(SwingConstants.LEFT);
        textTogether.setVerticalAlignment(SwingConstants.CENTER);
        this.add(textTogether);


        for (int i = 0; i < gridSizeX; i++) {
            for (int j = 0; j < gridSizeY; j++) {
                data_array[i][j] = new FireData(0, 0, 0, 0, 0);
            }
        }
    }


    @Override
    public void actionPerformed(ActionEvent actionEvent) {

    }

    @Override
    public void mouseClicked(MouseEvent mouseEvent) {

    }

    @Override
    public void mousePressed(MouseEvent mouseEvent) {
        selectedX = mouseEvent.getX() / differenceX;
        selectedY = mouseEvent.getY() / differenceY;

        if (!(validation(selectedX, selectedY))) {
            selectedX = -1;
            selectedY = -1;
        }

        repaint();
    }

    @Override
    public void mouseReleased(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseEntered(MouseEvent mouseEvent) {

    }

    @Override
    public void mouseExited(MouseEvent mouseEvent) {

    }

    @Override
    public Dimension getPreferredSize() {
        return new Dimension(winSizeX, winSizeY);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));
        drawGrid(g2);
        drawInfobox(g2);
        drawInfoFireTeamBox(g2);
        addButtons();
    }

    private void drawGrid(Graphics2D g2) {
        g2.drawImage(forest, 0, 0, imageSizeX, imageSizeY, null);
        g2.setColor(new Color(255, 255, 255));

        //grid fo r x
        for (int i = 0; i < (gridSizeX + 1); i++) {
            g2.drawLine(differenceX * i, 0, differenceX * i, imageSizeY);
        }

        //grid for y
        for (int i = 0; i < (gridSizeY + 1); i++) {
            g2.drawLine(0, i * differenceY, imageSizeX, i * differenceY);
        }

        g2.setColor(new Color(255, 0, 0));

        for (int i = 0; i < gridSizeX; i++) {
            for (int j = 0; j < gridSizeY; j++) {
                /*
                if (data_array[i][j].getStatus() > statusLimit) {
                    g2.drawRect(i * 100 + 5, j * 75 + 5, 90, 65);
                }
                 */
                if (data_array[i][j].getTemperature() > temperatureLimit) {
                    g2.drawRect(i * 100 + 5, j * 75 + 5, 90, 65);
                }
            }
        }
    }

    private void drawInfobox(Graphics2D g2) {
        g2.setColor(new Color(0, 0, 0));
        g2.drawRect(1000, 0, 200, 200);

        if (selectedX > -1 && selectedY > -1) {

            String info = "<html> Selected place value: " +
                    "<br> Temperature: " + data_array[selectedX][selectedY].getTemperature() +
                    "<br> Status: " + data_array[selectedX][selectedY].getStatus() +
                    "<br> Id: " + data_array[selectedX][selectedY].getId() +
                    "</html>";

            text.setText(info);

            g2.setColor(new Color(100, 100, 250));
            g2.drawRect(selectedX*100 + 10, selectedY * 75 + 10,80, 55);
        } else {
            text.setText("Select field to show more info");
        }
    }

    private void drawInfoFireTeamBox(Graphics2D g2){

        g2.setColor(new Color(0, 0, 0));
        g2.drawRect(1000, 200, 200, 400);

        String fireTeam = "<html> &emsp TEAM FIRE INFORMATION <br>" +
                "<br> &emsp Units: " + this.action_f +
                "</html>";

        textFireTeam.setText(fireTeam);

        String patrolTeam = "<html> &emsp PATROL INFORMATION <br>" +
                "<br> &emsp Units: " + this.action_p + "</html>";

        textPatrolTeam.setText(patrolTeam);

        String together = "<html> &emsp RAZME <br>" +
                "<br> &emsp Units: " + (this.action_p + this.action_f)+ "</html>";

        textTogether.setText(together);
    }

    private void addButtons(){
        int x = 1020;
        int y = 702;
        int width = 165;
        int height = 20;
        int padding = 30;

        this.button1 = new JButton("SEND PATROL");
        this.button2 = new JButton("SEND FIRE TEAM");
        this.button3 = new JButton("RESET PATROL");
        this.button4 = new JButton("RESET FIRE TEAM");
        this.button5 = new JButton("GENERATE PDF");

        this.button4.setBounds(x,y,width,height);
        this.button3.setBounds(x,y-padding+3,width,height);
        this.button2.setBounds(x,y-padding*2+3,width,height);
        this.button1.setBounds(x,y-padding*3+3,width,height);
        this.button5.setBounds(x,y+padding-3,width,height);

        add(button4);
        add(button3);
        add(button1);
        add(button2);
        add(button5);



        button1.addActionListener(e->{

                this.action_p++;

                TeamDto fireDto = new TeamDto(this.available_f,this.action_f,this.action_f);
                TeamDto patrolDto = new TeamDto(this.available_p,this.action_p,this.all_action_p);

                int id;
                int posistionId = selectedX * 10 + selectedY;
                List dataInfo = this.jsonData.getJson();
                List dataRegion = new ArrayList();
                for (int i = 0; i < 10; i++) {

                    for (int j = 0; j < 10; j++) {
                        id = i * 10 + j;

                        JSONObject dataArea = (JSONObject) dataInfo.get(id);

                        int id_json = Integer.parseInt(dataArea.get("id").toString());
                        int state_json = Integer.parseInt(dataArea.get("state").toString());
                        float temperature_json = Float.parseFloat(dataArea.get("temperature").toString());

                        FireData fireData = new FireData(temperature_json, 0, 0, id_json, state_json);

                        fireData.setX(i);
                        fireData.setY(j);
                        if (id == posistionId && state_json > 5) {
                            fireData.setStatus(0);
                            fireData.setTemperature(20); //Trzeba zrobic cos pod ustawianie temperatury

                            patrolDto.setUnits(this.available_p);
                            patrolDto.setAction(this.action_p);

                            fireDto.setAction(this.action_f);
                            fireDto.setUnits(this.available_f);
                            fireDto.setAll_action(this.all_action_f);
                        }
                        Data data = new Data(fireData.getId(), fireData.getStatus(), fireData.getTemperature());
                        dataRegion.add(data);
                        setData(i, j, fireData);
                    }
                }
                jsonData.writeJson(dataRegion);
                this.json.writeJsonTeam(fireDto,patrolDto);
        });

        button2.addActionListener(e->{
                this.action_f++;

                TeamDto fireDto = new TeamDto(this.available_f, this.action_f, this.action_f);
                TeamDto patrolDto = new TeamDto(this.available_p, this.action_p, this.all_action_p);

                this.json.writeJsonTeam(fireDto, patrolDto);

                int id;
                int posistionId = selectedX * 10 + selectedY;
                List dataInfo = this.jsonData.getJson();
                List dataRegion = new ArrayList();
                for (int i = 0; i < 10; i++) {

                    for (int j = 0; j < 10; j++) {
                        id = i * 10 + j;

                        JSONObject dataArea = (JSONObject) dataInfo.get(id);

                        int id_json = Integer.parseInt(dataArea.get("id").toString());
                        int state_json = Integer.parseInt(dataArea.get("state").toString());
                        float temperature_json = Float.parseFloat(dataArea.get("temperature").toString());

                        FireData fireData = new FireData(temperature_json, 0, 0, id_json, state_json);

                        fireData.setX(i);
                        fireData.setY(j);
                        if (id == posistionId) {
                            fireData.setStatus(0);
                            fireData.setTemperature(20); //Trzeba zrobic cos pod ustawianie temperatury
                        }
                        Data data = new Data(fireData.getId(), fireData.getStatus(), fireData.getTemperature());
                        dataRegion.add(data);
                        setData(i, j, fireData);
                    }
                }
                jsonData.writeJson(dataRegion);
        });

        button3.addActionListener(e->{
            this.action_f = 0;

            TeamDto fireDto = new TeamDto(this.available_f,this.action_f,this.action_f);
            TeamDto patrolDto = new TeamDto(this.available_p,this.action_p,this.all_action_p);

            this.json.writeJsonTeam(fireDto, patrolDto);

        });

        button4.addActionListener(e->{
            this.action_p = 0;

            TeamDto fireDto = new TeamDto(this.available_f,this.action_f,this.action_f);
            TeamDto patrolDto = new TeamDto(this.available_p,this.action_p,this.all_action_p);

            this.json.writeJsonTeam(fireDto, patrolDto);
        });

        button5.addActionListener(e->{
            List dataList = this.jsonData.getJson();
            CreatorPDF creatorPDF = new CreatorPDF(dataList);
        });
    }


    public void setData(int x, int y, FireData data) {
        if (validation(x, y)) {
            data_array[x][y] = data;
        }
    }

    public void setData_array(FireData data[][], int x, int y) {

        if(!(validation(x, y))) {
            System.out.println("Excaption wrong size of array");
            return;
        }

        for (int i = 0; i < gridSizeX; i++) {
            for (int j = 0; j < gridSizeY; j++) {

                data_array[i][j] = data[i][j];
            }
        }
    }


    private boolean validation(int x, int y) {
        if (0 > x || x >= gridSizeX) {
            return false;
        }

        if (0 > y || y >= gridSizeY) {
            return false;
        }

        return true;
    }
}
