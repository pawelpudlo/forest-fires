package DTO;

public class FireData {

    private float Temperature;
    private int x, y, Id, status;

    public FireData(float temperature, int x, int y, int id, int status) {
        Temperature = temperature;
        this.x = x;
        this.y = y;
        Id = id;
        this.status = status;
    }

    public int getStatus() {
        return status;
    }

    public float getTemperature() {
        return Temperature;
    }

    public void setTemperature(float temperature) {
        Temperature = temperature;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
