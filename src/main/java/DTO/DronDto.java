package DTO;

public class DronDto {

    private int idDron;
    private boolean status;
    private int battery_level;
    private boolean failure;
    private boolean loading;
    private int start_place;
    private int final_place;
    private int current_place;

    public DronDto(int idDron, boolean status, int battery_level, boolean failure, boolean loading, int start_place, int final_place, int current_place) {
        this.idDron = idDron;
        this.status = status;
        this.battery_level = battery_level;
        this.failure = failure;
        this.loading = loading;
        this.start_place = start_place;
        this.final_place = final_place;
        this.current_place = current_place;
    }

    public void setIdDron(int idDron) {
        this.idDron = idDron;
    }

    public void setCurrent_place(int current_place) {
        this.current_place = current_place;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public void setBattery_level(int battery_level) {
        this.battery_level = battery_level;
    }

    public void setFailure(boolean failure) {
        this.failure = failure;
    }

    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    public void setStart_place(int start_place) {
        this.start_place = start_place;
    }

    public void setFinal_place(int final_place) {
        this.final_place = final_place;
    }

    public int getIdDron() {
        return idDron;
    }

    public boolean isStatus() {
        return status;
    }

    public int getBattery_level() {
        return battery_level;
    }

    public boolean isFailure() {
        return failure;
    }

    public int getCurrent_place() {
        return current_place;
    }

    public boolean isLoading() {
        return loading;
    }

    public int getStart_place() {
        return start_place;
    }

    public int getFinal_place() {
        return final_place;
    }
}
