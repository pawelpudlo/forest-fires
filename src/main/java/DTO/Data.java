package DTO;

public class Data {

    private int id;
    private int state;
    private float temperature;

    public Data(int id, int state, float temperature) {
        this.id = id;
        this.state = state;
        this.temperature = temperature;
    }

    public int getId() {
        return id;
    }

    public int getState() {
        return state;
    }

    public float getTemperature() {
        return temperature;
    }
}
