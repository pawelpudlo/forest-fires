package DTO;

public class AllTeamDto {

    private TeamDto teamFire;
    private TeamDto teamPatrol;

    public AllTeamDto(TeamDto teamFire, TeamDto teamPatrol) {
        this.teamFire = teamFire;
        this.teamPatrol = teamPatrol;
    }

    public TeamDto getTeamFire() {
        return teamFire;
    }

    public void setTeamFire(TeamDto teamFire) {
        this.teamFire = teamFire;
    }

    public TeamDto getTeamPatrol() {
        return teamPatrol;
    }

    public void setTeamPatrol(TeamDto teamPatrol) {
        this.teamPatrol = teamPatrol;
    }
}
