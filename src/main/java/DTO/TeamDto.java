package DTO;

public class TeamDto {

    private int units;
    private int action;
    private int all_action;

    public TeamDto(int units, int action, int all_action) {
        this.units = units;
        this.action = action;
        this.all_action = all_action;
    }

    public int getUnits() {
        return units;
    }

    public void setUnits(int units) {
        this.units = units;
    }

    public int getAction() {
        return action;
    }

    public void setAction(int action) {
        this.action = action;
    }

    public int getAll_action() {
        return all_action;
    }

    public void setAll_action(int all_action) {
        this.all_action = all_action;
    }
}
